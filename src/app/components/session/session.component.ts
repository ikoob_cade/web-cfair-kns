import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line: no-input-rename
  @Input('session') session: any; // 세션 정보
  @Output('detailFn') detailFn = new EventEmitter(); // 자세히보기
  
  public polling: any;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    clearTimeout(this.polling);
  }

}
