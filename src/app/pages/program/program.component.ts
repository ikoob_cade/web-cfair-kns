import { Component, OnInit } from '@angular/core';
import { VodService } from 'src/app/services/api/vod.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  public vods: any[] = [];
  public categories: any;
  public excerptUrl = 'https://cfair-files.s3.ap-northeast-2.amazonaws.com/events/5f7d2bd00c3ed100114bd66d/%E1%84%83%E1%85%A2%E1%84%92%E1%85%A1%E1%86%AB%E1%84%89%E1%85%B5%E1%86%AB%E1%84%80%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%AC%E1%84%80%E1%85%AA%E1%84%92%E1%85%A1%E1%86%A8%E1%84%92%E1%85%AC_2020+KNS+%E1%84%8E%E1%85%AE%E1%84%80%E1%85%A8+%E1%84%91%E1%85%B3%E1%84%85%E1%85%A9%E1%84%80%E1%85%B3%E1%84%85%E1%85%A2%E1%86%B7%E1%84%87%E1%85%AE%E1%86%A8.pdf';
  constructor(
    private router: Router,
    private vodService: VodService
  ) {
    this.getVod();
  }

  ngOnInit(): void {
  }

  public getVod(): void {
    this.vodService.find().subscribe((response: any) => {
      this.categories = this.sortByCategory(response);
      // this.vods = response;
    });
  }

  public goDetail(vod:any): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.vods = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

}
